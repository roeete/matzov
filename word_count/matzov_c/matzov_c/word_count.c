#include <stdio.h>
#include <stdlib.h>

//Everything is so complicated with this C thing. Let's return the records and go back to Nethanya.

const char UPPERCASE_LETTERS[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
const char LOWERCASE_LETTERS[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
struct node {
	// The node for the linked list.
	char* word;
	struct node* nxt;
	int count;
};

int is_char_valid(char c) {
	//This function returns 1 if the char passed as an argument is either a lower or upper case letter. Otherwise it returns 0. 
	for (int i = 0; i < sizeof(UPPERCASE_LETTERS) / sizeof(UPPERCASE_LETTERS[0]); i++) {
		if (c == UPPERCASE_LETTERS[i])
			return 1;
	}

	for (int i = 0; i < sizeof(LOWERCASE_LETTERS) / sizeof(LOWERCASE_LETTERS[0]); i++) {
		if (c == LOWERCASE_LETTERS[i])
			return 1;
	}

	return 0;
}

char lowercase(char c) {
	//If c provided as an argument is an upper-case letter, this function return the corresponding lowercase letter.
	for (int i = 0; i < sizeof(UPPERCASE_LETTERS) / sizeof(UPPERCASE_LETTERS[0]); i++) {
		if (c == UPPERCASE_LETTERS[i])
			return LOWERCASE_LETTERS[i];
	}
	return c;
}

int search_list(struct node* h, char* word) {
	//This function returns a node in h (linked list) that matches the provided word. If there is none, returns NULL.
	struct node* c = h;
	while (c != NULL) {
		if (c->word != NULL && strcmp(c->word, word) == 0)
			return c;
		c = c->nxt;
	}
	return NULL;
}

int count_list(struct node* h) {
	//This functions return how many nodes there are in the linked list that is passed as h
	struct node* c = h;
	int count = 0;
	while (c != NULL) {
		count++;
		c = c->nxt;
	}
	return count;
}

struct node* bubble_sort_list(struct node* h) {
	//This function orders the linked list alphabeticly by word using bubble sort. 
	int left_to_sort = count_list(h);
	if (left_to_sort < 2)
		return h;

	struct node* current;
	struct node* previous;
	struct node* double_previous;

	//Bubble sort implementation. This is a mess.
	while (left_to_sort > 1) {
		current = h->nxt->nxt;
		previous = h->nxt;
		double_previous = h;
		for (int i = 2; i < left_to_sort - 1; i++) {
			if (strcmp(previous->word, current->word) > 0) {
				double_previous->nxt = current;
				previous->nxt = current->nxt;
				current->nxt = previous;

				double_previous = current;
				current = previous->nxt;
			}
			else {
				double_previous = previous;
				previous = current;
				current = current->nxt;
			}
		}
		left_to_sort--;
	}

	//Putting the head of the list in its right place
	current = h->nxt;
	previous = h;
	struct node* tmp;
	//Finding the sandwhich position that h should be put in:
	while (strcmp(h->word, current->word) > 0) {
		previous = current;
		current = current->nxt;
	}
	//If h should be moved, move it and advance the root to the right word.
	if (previous != h) {
		tmp = h->nxt;
		previous->nxt = h;
		h->nxt = current;
		h = tmp;
	}

	return h;
}

main() {

	// Opening the file
	FILE* fp;
	fp = fopen("C:\\Users\\admin\\Desktop\\matzov\\aladdin.txt", "r");
	if (fp == NULL) {
		printf("There was an error reading the file");
		exit(0);
	}

	//Count chars
	char c = fgetc(fp);
	int num_of_chars = 0;
	while (c != EOF) {
		num_of_chars++;
		c = fgetc(fp);
	}

	//Read chars
	fseek(fp, 0, SEEK_SET);
	char *f_chars = (char *)malloc(num_of_chars * sizeof(char));
	int c_index = 0;
	int added_space = 0; // To prevent double spaces 

	//We are writing the chars to f_chars so we don't have to deal with double spaces, special chars, new lines and so on.
	c = fgetc(fp);
	while (c != EOF) {
		//If the current char is a char that dictates a single white space, add a whitespace only if we have not already appended a whitespace (we don't want consecutive spaces)
		if ((c == ' ' || c == '\n' || c == '-') && added_space == 0) {
			added_space = 1;
			f_chars[c_index] = ' ';
			c_index++;
		}

		//If the current char is not a space and is a valid char (an alphabetic char), add it.
		if(c != ' ' && c != '\n' && c != '-') {
			if (is_char_valid(c) == 1) {
				f_chars[c_index] = lowercase(c);
				added_space = 0;
				c_index++;
			}
		}
		c = fgetc(fp);
	}

	//Link unique words
	char previous_char;
	char word[100] = "";
	struct node* h = malloc(sizeof(struct node));
	h->word = NULL;
	h->count = 0;
	h->nxt = NULL;
	struct node* current = h;
	struct node* already_in_list;
	int w_counter = 0;

	//We are iterating through every char, while keeping the previous char and current char.
	for (int i = 1; i < num_of_chars; i++) {
		previous_char = f_chars[i - 1];
		c = f_chars[i];
		//If this is true than it means we have a word! Deal with the word.
		if (c == ' ' && previous_char != ' ') {
			//Check if the word has already been counted once.
			already_in_list = search_list(h, word);
			if (already_in_list != NULL) {
				//If so, just amp up its counter.
				already_in_list->count += 1;
			}
			else {
				//If it doesn't, append it to the end of the list with the counter of 1
				current->word = malloc(sizeof(word));
				strcpy(current->word, word);
				current->nxt = malloc(sizeof(struct node));
				current = current->nxt;
				current->count = 1;
				current->word = NULL;
				current->nxt = NULL;
			}

			//Reset the buffer in which we save the word:
			memset(word, 0, 100);
			w_counter = 0;
		}
		else {
			//If this char is valid append it to the current word buffer. This check for validity is redudant since we only add valid chars to f_chars, but just in case.
			//Our buffer is 100 bytes size so if we have a really long word we have a problem and we need to terminate our program.
			if (is_char_valid(c) == 1) {
				if (w_counter > 100) {
					printf("Wow wow wow!! Mr. Aladin.. Your word is too long for me. BAD ALADIN, SAD. VERY SAD"); //Mr Trump you are my hero
					exit(1);
				}
				word[w_counter] = c;
				w_counter++;
			}
		}
	}

	//Ordering the list alphabeticlly and than printing.
	h = bubble_sort_list(h);
	printf("\n-------------------------------\nNumber of unique words: %d\n-------------------------------\nList of words and count:\n-------------------------------\n", count_list(h));
	current = h;
	int i = 1;
	while (current->nxt != NULL) {
		printf("%d. %s: %d\n", i, current->word, current->count);
		current = current->nxt;
		i++;
	}
	fclose(fp);
}