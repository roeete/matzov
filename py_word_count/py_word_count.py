import sys


def print_words_list(words, word_count):
    '''
    Prints a list of words and the number of their occurrences according to their order in words
    :param (list of strings) words: the list of words ordered by the wanted order to print
    :param (dict[string] -> int)word_count: the dictionary contaning the number of occurrences for each word
    :return: None
    '''
    for word in words:
        print(word+": "+str(word_count[word]))


def count_words(filename):
    '''
    For a given file, return a dictionary which describes the number of occurrences for each unique word.
    :param string filename: The path to the file
    :return dictionary[string] -> int: The counted dictionary
    '''
    word_count = {}
    with open(filename, "r") as f:
        words = list(map(lambda w: w.lower(),
                         filter(lambda w: w != '', f.read().replace("\n", ' ').replace("-", " ").split(" ")))) # Doing a
        # whole bunch of thing in a one liner: making every word a lower-case, replacing new line with whitespaces,
        # replacing - with whitespaces, and also reading the entire file :)
        words = list(map(lambda w: ''.join(list(filter(lambda c: c.isalpha(), w))), words)) # or c == "'"
        # replacing all special chars with nothing so that how? will be counted with how and etch.
        for word in words: # counting the words
            if not word in word_count:
                word_count[word] = 1
            else:
                word_count[word] += 1
    return word_count


def print_words(word_count):
    '''
    Prints words_count dictionary by *alphabetical order*
    :param (dictionary[string] -> int) word_count: A dictionary that describes the number of occurrences for each word
    :return: None
    '''
    words = sorted(word_count.keys())
    print_words_list(words, word_count)


def sort_by_appearances(word_count):
    '''
    Sorting the word count dict by how many times each word appears in the text. For words with the same amount of
    appearances, sorting by alphabetical order.
    :param (dictionary[string] -> int) word_count: A dictionary that describes the number of occurrences for each word
    :return: The sorted dict
    '''
    by_appearances = [word[0] for word in sorted(word_count.items(), key=lambda w: (-w[1], w[0]), reverse=False)]
    return by_appearances


def print_top(word_count):
    '''
    For a word_count dictionary prints the top 20 most common words
    :param (dictionary[string] -> int) word_count: A dictionary that describes the number of occurrences for each word
    :return: None
    '''
    # Calling sort_by_appearances but taking only the 20 highest, and again into a dict
    print_words_list(sort_by_appearances(word_count)[:20], word_count)


def main():
    if len(sys.argv) != 3 and len(sys.argv) != 4:
        print("usage: ./wordcount.py {--count --topcount} file")
        sys.exit(1)

    # This could be done much prettier by implementing optparse or by writing my own version of optpars
    if len(sys.argv) == 3:  # Parsing the options and user input
        option = sys.argv[1]
        filename = sys.argv[2]
        if option == '--count':
            word_count = count_words(filename)
            print_words(word_count)
        elif option == '--topcount':
            word_count = count_words(filename)
            print_top(word_count)
    elif len(sys.argv) == 4:
        option1 = sys.argv[1]
        option2 = sys.argv[2]
        filename = sys.argv[3]
        if (option1 == '--count' and option2 == "--topcount") or (option1 == "--topcount" and option2 == "--count"):
            word_count = count_words(filename)
            print("\n-----------\nCount\n-----------\n")
            print_words(word_count)
            print("\n-----------\nTop Count\n-----------\n")
            print_top(word_count)
        elif option1 == '--count' and (option2 == '--letter' or option2 == '--appearance'):
            word_count = count_words(filename)
            if option2 == '--letter':
                # Printing word_count dict by alphabetical order
                alphabetical_words = [word[0] for word in sorted(word_count.items(), key=lambda w: w[0], reverse=False)]
                print_words_list(alphabetical_words, word_count)
            else:
                # Printing word count by amount of appearances and alphabetical order
                print_words_list(sort_by_appearances(word_count), word_count)
        else:
            print("unknown option/s: ")
            sys.exit(1)
    else:
        print("unknown option/s: ")
        sys.exit(1)


if __name__ == '__main__':
    main()

